<?php
//namespace Buffon;
/**
 * Description of Logger
 * 
 * стандартный класс логирования
 * создает две папки (errors и logs) 
 * рядом с исполняемым файлом
 * с соответствующим содержанием
 * 
 * объявление Logger::logSetup(__FILE__);
 * 
 * @author Buffon
 */
class Logger {
    
    static $status = true;
    static $patch;
    static $name;
    static $logClient = '?';
    
    /**
     * включить/выключить логирование
     * поумолчанию логирование включено
     * @param boolean $status
     */
    public static function status(bool $status) {
        self::$status = $status;
    }
    
    /**
     * указать имя фаила
     * @param string $name
     */
    public static function logSetup($name) {
        if (self::$status) {
            $dir = dirname($name);
            self::$patch = $dir . '/logs/';
            if (!is_dir(self::$patch)) {
                mkdir(self::$patch, 0777);
            }
            self::errorLog($dir);
            self::$name = date("Ymd_").basename($name, ".php");
        }
    }
    
    /**
     * запись в фаил лога
     * @param type $data
     */
    public static function log($data) {
        if (self::$status) {
            $s = date("Y.m.d H:i:s")." ip:".self::getUserIp().' '.self::$logClient.' ';
            $s .= print_r($data,true);
            error_log($s . PHP_EOL, 3, self::$patch . self::$name . '.log');
        }
    }
    
    /**
     * включить логирование ошибок
     * @param string $dir
     */
    private static function errorLog ($dir) {
        if (self::$status) {
            $dir .= '/errors/';
            if (!is_dir($dir)) {
                mkdir($dir, 0777);
            }
            ini_set('error_log', $dir. '/php_errors_' . date("Ymd", time()) . '.log');
            ini_set('log_errors', 1);
        }
    }
    
    public static function getUserIp() {
        $ip = null;
        if (!empty($_SERVER['HTTP_X_REAL_IP'])) {
          $ip = $_SERVER["HTTP_X_REAL_IP"];
        } elseif (!empty($_SERVER['HTTP_CLIENT_IP'])){
          $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
          $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
          $ip = isset($_SERVER['REMOTE_ADDR'])?$_SERVER['REMOTE_ADDR']:'127.0.0.1';
        }
        return $ip;
    }
  
    public static function logClient($client) {
        self::$logClient = $client;
    }
}
